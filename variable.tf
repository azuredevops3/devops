variable "prefix" {
  type = string
  description = "This prefix will be added to all Azure resources"
}

variable "tags" {
    type = map
    description = "Tags for the Azure resources"
}

variable "location" {
   type = string
   description = "Location for the Azure resource"
}

variable "sku" {
    type = map
    description = "Azure VM Image Sku to be decided based on the region"
}

variable "admin_username" {
    type = string
    description = "Azure VM admin username"
}

variable "admin_password" {
    type = string
    description = "Azure VM admin username"
}

variable "caching" {
    type = string
    description = "Disk Caching Read or ReadWrite"
}

variable "storage_account_type" {
    type = string
    description = "Azure Storage account type like Standard LRS or GRS"
}


