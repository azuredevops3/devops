prefix                  = "tfdemo11"
tags                    = {
            Environment = "Sandbox"
            Department  = "Sales"
            }
location                = "eastus"
sku                     = {
                 westus = "2019-Datacenter"
                 eastus = "2016-Datacenter"
                }
admin_username          = "tfdemo11admin"
admin_password          = "Tfdemo11@Admin999"
caching                 = "ReadWrite"
storage_account_type    = "Standard_LRS"