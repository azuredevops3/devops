# Start with Terraform block and define the required terraform version to be used
terraform {
  # Next use the required providers block and mention the source and version of the azurerm provider
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = ">=2.46.0"
    }
  }
  required_version = ">=0.14.9"
}

provider "azurerm" {
 features {}
}
##############################################################################################################
# 1) Create Azure Resource Group
resource "azurerm_resource_group" "tfdemo11rg" {
  name     = "${var.prefix}-rg"
  location = var.location
}
##############################################################################################################
# 2) Create Azure Virtual Network
resource "azurerm_virtual_network" "tfdemo11vnet" {
  name                = "${var.prefix}-vnet"
  location            = azurerm_resource_group.tfdemo11rg.location
  address_space       = [ "10.0.0.0/24" ]
  resource_group_name = azurerm_resource_group.tfdemo11rg.name
}
###############################################################################################################
# 3) Create Azure Subnet
resource "azurerm_subnet" "tfdemo11subnet" {
  name                 = "${var.prefix}-subnet"
  address_prefixes     = [ "10.0.0.0/25" ]
  virtual_network_name = azurerm_virtual_network.tfdemo11vnet.name
  resource_group_name  = azurerm_resource_group.tfdemo11rg.name
}
###############################################################################################################
# 4) Create Azure public ip resource
resource "azurerm_public_ip" "tfdemo11publicip" {
 name                  = "${var.prefix}-publicip"  
 location              = azurerm_resource_group.tfdemo11rg.location
 resource_group_name   = azurerm_resource_group.tfdemo11rg.name
 allocation_method     = "Static" # This is case sensitive, marking this as static will result in an error
}
###############################################################################################################
# 5) Create Azure Network Interface
resource "azurerm_network_interface" "tfdemo11nic" {
  name                = "${var.prefix}-nic"
  location            = azurerm_resource_group.tfdemo11rg.location
  resource_group_name = azurerm_resource_group.tfdemo11rg.name
  ip_configuration {
    name                          = "tfdemo11configuration"
    subnet_id                     = azurerm_subnet.tfdemo11subnet.id
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = azurerm_public_ip.tfdemo11publicip.id
    }
}
###############################################################################################################
# 6) Create Azure Network Security Group with rules
resource "azurerm_network_security_group" "tfdemo11nsg" {
  name                         = "${var.prefix}-nsg"
  location                     = azurerm_resource_group.tfdemo11rg.location
  resource_group_name          = azurerm_resource_group.tfdemo11rg.name
  # security_rule is a list of objects. Fixed collection of objects. List of objects assigned to argument.
  security_rule {
    access                     = "allow"
    description                = "RDP security group"
    destination_address_prefix = "*"
    destination_port_range     = "3389"
    direction                  = "inbound"
    name                       = "AllowRDP"
    priority                   = 100
    protocol                   = "TCP"
    source_address_prefix      = "*"
    source_port_range          = "*"
  }
}
###############################################################################################################
# 7) Create Network Security group association with the Network Interface Card
resource "azurerm_network_interface_security_group_association" "tfdemo11asc" {
  network_interface_id         = azurerm_network_interface.tfdemo11nic.id
  network_security_group_id    = azurerm_network_security_group.tfdemo11nsg.id
}
################################################################################################################
# 8) Create a Windows Virtual Machine
resource "azurerm_windows_virtual_machine" "tfdemo11vm" {
  name                        = "${var.prefix}-vm1"
  resource_group_name         = azurerm_resource_group.tfdemo11rg.name
  location                    = azurerm_resource_group.tfdemo11rg.location
  size                        = "Standard_DS1_V2"
  network_interface_ids       = [ azurerm_network_interface.tfdemo11nic.id ]
  admin_username              = var.admin_username
  admin_password              = var.admin_password

  os_disk {
    caching                   = var.caching
    storage_account_type      = var.storage_account_type
  }

  source_image_reference {
    publisher                 = "MicrosoftWindowsServer"
    offer                     = "WindowsServer"
    sku                       = lookup(var.sku,var.location)
    version                   = "latest"
  }
  tags = var.tags
}
  data "azurerm_public_ip" "ip"{
    name                      = azurerm_public_ip.tfdemo11publicip.name
    resource_group_name       = azurerm_windows_virtual_machine.tfdemo11vm.resource_group_name
    depends_on                = [azurerm_windows_virtual_machine.tfdemo11vm]
      }